<?php
/* uDispatch
 * Copyright (c) 2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Test class for uDispatch routes
 *
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 2.0.0
 */
class RouteTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \uDispatch\Route
	 */
	private $route;

	/**
	 * Default callback
	 * @var callable
	 */
	private $callback;

	public function setUp()
	{
		$this->route = new \uDispatch\Route();
		$this->callback = function()
			{
				return true;
			};
	}

	/**
	 * The run() method needs to return what the callback returns or, if the callback is not set, the params array.
	 * On unsuccessful URI match, false must be returned.
	 */
	public function testCallbackReturn()
	{
		$this->route
			->match("/");

		$params = $this->route->run("/", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(0, $params);

		$this->route
			->callback(function($params) {
				return "lumberjack";
			});

		$this->assertEquals("lumberjack", $this->route->run("/", "GET"));

		$this->assertFalse($this->route->run("/buttered/scones", "GET"));
	}

	/**
	 * Test the route with default settings
	 */
	public function testDefaults()
	{
		// no callback is set, no params returned
		$result = $this->route->run("/", "POST");
		$this->assertInternalType("array", $result);
		$this->assertCount(0, $result);

		$this->route->callback($this->callback);

		// wrong URI
		$this->assertFalse($this->route->run("/some/uri", "POST"));

		// correct default URI, any method is OK
		$this->assertTrue($this->route->run("/", "POST"));
		$this->assertTrue($this->route->run("/", "GET"));
		$this->assertTrue($this->route->run("/", "PUT"));
		$this->assertTrue($this->route->run("/", "DELETE"));
	}

	/**
	 * Make sure that limiting the request methods works
	 */
	public function testMethods()
	{
		$this->route
			->method(["PUT", "DELETE"])
			->callback($this->callback);

		$this->assertFalse($this->route->run("/", "POST"));
		$this->assertFalse($this->route->run("/", "GET"));
		$this->assertTrue($this->route->run("/", "PUT"));
		$this->assertTrue($this->route->run("/", "DELETE"));
	}

	/**
	 * It must be possible to allow protocol checking
	 */
	public function testProtocol()
	{
		$this->route
			->callback($this->callback);

		$this->assertTrue($this->route->run("/", "GET"));
		$this->assertTrue($this->route->run("/", "GET", "HTTP"));
		$this->assertTrue($this->route->run("/", "GET", "HTTPS"));

		$this->route
			->protocol("HTTP");

		$this->assertTrue($this->route->run("/", "GET"));
		$this->assertTrue($this->route->run("/", "GET", "HTTP"));
		$this->assertFalse($this->route->run("/", "GET", "HTTPS"));

		$this->route
			->protocol("HTTPS");

		$this->assertTrue($this->route->run("/", "GET"));
		$this->assertFalse($this->route->run("/", "GET", "HTTP"));
		$this->assertTrue($this->route->run("/", "GET", "HTTPS"));
	}

	/**
	 * Static URIs must be resolved easily
	 */
	public function testUri()
	{
		$this->route
			->match("/lumberjack")
			->callback($this->callback);

		$this->assertFalse($this->route->run("/", "GET"));
		$this->assertTrue($this->route->run("/lumberjack", "GET"));
	}

	/**
	 * URIs containing parametres should be resolved correctly, and parametre values should
	 * be passed to the callback functions.
	 */
	public function testUriParams()
	{
		$this->route
			->match("/lumberjack/{id}");

		$this->assertFalse($this->route->run("/lumberjack", "GET"));

		$params = $this->route->run("/lumberjack/1", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(1, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertEquals("1", $params["id"]);

		// test two params
		$this->route
			->match("/lumberjack/{id}/wear/{clothing}");

		$this->assertFalse($this->route->run("/lumberjack", "GET"));
		$this->assertFalse($this->route->run("/lumberjack/1", "GET"));
		$this->assertFalse($this->route->run("/lumberjack/1/wear", "GET"));

		$params = $this->route->run("/lumberjack/1/wear/high-heels-suspenders-and-a-bra", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(2, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertEquals($params["id"], "1");
		$this->assertArrayHasKey("clothing", $params);
		$this->assertEquals("high-heels-suspenders-and-a-bra", $params["clothing"]);
	}

	/**
	 * Custom parametres must work with custom matching patterns
	 */
	public function testUriParamsWithAssertions()
	{
		// numeric param
		$params = [];
		$this->route
			->match("/lumberjack/{id}")
			->assert("id", "[1-9][0-9]*");

		$this->assertFalse($this->route->run("/lumberjack", "GET"));
		$this->assertFalse($this->route->run("/lumberjack/hang-around-in-bars", "GET"));

		$params = $this->route->run("/lumberjack/123", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(1, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertEquals("123", $params["id"]);

		// URL alias
		$params = [];
		$this->route
			->match("/{alias}")
			->assert("alias", "[a-z0-9\\/\\-]+");

		$params = $this->route->run("/2013/09/im-a-lumberjack-and-im-ok", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(1, $params);
		$this->assertArrayHasKey("alias", $params);
		$this->assertEquals("2013/09/im-a-lumberjack-and-im-ok", $params["alias"]);
	}

	/**
	 * It must be possible to convert params to any data type
	 */
	public function testUriParamsWithConverters()
	{
		$params = [];
		$this->route
			->match("/{date}/{id}/{alias}")
			->assert("date", "\\d{4}\\/\\d{2}")
			->assert("alias", "[a-z0-9\\/\\-]+")
			->convert("date", function($date) {
				$date = explode("/", $date);
				$return = new \DateTime();
				$return->setDate($date[0], $date[1], 1);
				return $return;
			})
			->convert("id", function($id) {
				return intval($id);
			});

		$params = $this->route->run("/2013/09/71/im-a-lumberjack-and-im-ok", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(3, $params);
		$this->assertArrayHasKey("date", $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertArrayHasKey("alias", $params);

		$this->assertInstanceOf("DateTime", $params["date"]);
		$this->assertEquals("9 2013", $params["date"]->format("n Y"));
		$this->assertTrue(is_int($params["id"]));
		$this->assertEquals(71, $params["id"]);
		$this->assertEquals("im-a-lumberjack-and-im-ok", $params["alias"]);
	}

	/**
	 * It must be possible to set a default value for a parametre
	 */
	public function testUriParamsDefaults()
	{
		$params = [];
		$this->route
			->match("/lumberjack/{id}")
			->value("id", 1)
			->convert("id", function($id) {
				return intval($id);
			});

		$params = $this->route->run("/lumberjack/123", "GET");
		$this->assertInternalType("array", $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertEquals(123, $params["id"]);

		$params = $this->route->run("/lumberjack", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(1, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertEquals(1, $params["id"]);

		$this->route
			->match("/lumberjack/{id}.{format}")
			->value("id", 1)
			->value("format", "html");

		$params = $this->route->run("/lumberjack/123.json", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(2, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertArrayHasKey("format", $params);
		$this->assertEquals("123", $params["id"]);
		$this->assertEquals("json", $params["format"]);

		$params = $this->route->run("/lumberjack/123", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(2, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertArrayHasKey("format", $params);
		$this->assertEquals("123", $params["id"]);
		$this->assertEquals("html", $params["format"]);

		$params = $this->route->run("/lumberjack", "GET");
		$this->assertInternalType("array", $params);
		$this->assertCount(2, $params);
		$this->assertArrayHasKey("id", $params);
		$this->assertArrayHasKey("format", $params);
		$this->assertEquals(1, $params["id"]);
		$this->assertEquals("html", $params["format"]);
	}

	/**
	 * It should be possible to assemble an URL string based on provided parametres
	 */
	public function testUrlAssembling()
	{
		$this->route
			->match("/{date}/lumberjack/{id}/{alias}")
			->assert("date", "[12][0-9]{3}\\/[01][0-9]")
			->assert("id", "[1-9][0-9]*")
			->assert("alias", "[0-9a-z\\/\\-]+");

		$expected = "/2013/09/lumberjack/123/cut-down-trees/have-buttered-scones/wear-high-heels";
		$actual = $this->route->assemble([
			"date" => "2013/09",
			"id" => "123",
			"alias" => "cut-down-trees/have-buttered-scones/wear-high-heels"
		]);

		$this->assertEquals($expected, $actual);
	}

	/**
	 * If a parametre is missing, an URL cannot be assembled correctly. A suitable exception should be thrown.
	 */
	public function testUrlAssemblingWithMissingParametre()
	{
		$this->route
			->match("/{date}/lumberjack/{id}/{alias}");

		$this->setExpectedException("\\uDispatch\\RouteException", \uDispatch\RouteException::MISSING_ASSEMBLE_PARAMS);

		$this->route->assemble([]);
	}

	/**
	 * Default parametres should be omitted from the end of the URL.
	 * If a parametre is passed in and is equal to a default parametre, it should be omitted as well.
	 */
	public function testUrlAssemblingWithDefaults()
	{
		$this->route
			->match("/{date}/lumberjack/{id}/{alias}")
			->value("id", "123")
			->value("alias", "sleep-all-night-and-work-all-day")
		;

		$expected = "/2013/09/lumberjack";
		$actual = $this->route->assemble([
			"date" => "2013/09",
			"id" => "123"
		]);

		$this->assertEquals($expected, $actual);
	}

	/**
	 * All extra parametres passed to the route assembly method should become query string params.
	 * Query string params should have correct formatting.
	 */
	public function testRouteAssemblingWithQueryParams()
	{
		$this->route
			->match("/lumberjack/{id}")
			->value("id", "123");

		$expected = "/lumberjack/321?tea=buttered+scones";
		$actual = $this->route->assemble(["id" => 321, "tea" => "buttered scones"]);

		$this->assertEquals($expected, $actual);

		$expected = "/lumberjack?tea=buttered+scones";
		$actual = $this->route->assemble(["tea" => "buttered scones"]);

		$this->assertEquals($expected, $actual);
	}
}
