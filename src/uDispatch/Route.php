<?php
/* uDispatch
 * Copyright (c) 2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Significant parts of this file are based on the Route component of the Symfony framework.
 *
 * Copyright (c) 2004-2013 Fabien Potencier
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace uDispatch;

use uDispatch\RouteException;

/**
 * A route definition.
 *
 * @package uDispatch
 * @author Dominik Marczuk <mingos.nospam@gmail.com>
 * @since 2.0.0
 */
class Route
{
	const REGEX_DELIMITER = "#";

	/**
	 * This string defines the characters that are automatically considered separators in front of
	 * optional placeholders (with default and no static text following). Such a single separator
	 * can be left out together with the optional placeholder from matching and generating URLs.
	 */
	const SEPARATORS = "/,;.:-_~+*=@|";

	/**
	 * The protocol the router will work with. Empty array matches both HTTP and HTTPS.
	 * @var string
	 */
	private $protocol = "";

	/**
	 * Request URI to which to respond
	 * @var string
	 */
	private $uriPattern = "/";

	/**
	 * Accepted request methods. Empty array = all methods are accepted
	 * @var array
	 */
	private $method = [];

	/**
	 * Patterns for the parametres
	 * @var array
	 */
	private $assert = [];

	/**
	 * Closures used for converting parametres
	 * @var array
	 */
	private $convert = [];

	/**
	 * Default values for the URI params
	 * @var array
	 */
	private $value = [];

	/**
	 * Callback to execute if the route finds a match. The callback will receive the request params as arguments.
	 * @var callable
	 */
	private $callback = null;

	/**
	 * The compiled pattern
	 * @var array
	 */
	private $compiledPattern = [];

	/**
	 * Set the protocol the route will match
	 *
	 * @param string $protocol Either "HTTP", "HTTPS" or an empty string for both.
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @since 2.0.0
	 */
	public function protocol($protocol)
	{
		$this->protocol = $protocol;

		return $this;
	}

	/**
	 * Set the request URI to which the route should respond.
	 *
	 * It is possible to use variables, delimited by curly brackets, e.g. "/user/{user_id}". The name between brackets
	 * becomed the variable name.
	 *
	 * @param string $uri
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @since 2.0.0
	 */
	public function match($uri)
	{
		$this->uriPattern = $uri;
		$this->compiledPattern = [];

		return $this;
	}

	/**
	 * Set accepted request methods
	 *
	 * @param string|array $method Array of methods or a single method name
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @since 2.0.0
	 */
	public function method($method)
	{
		if (is_string($method)) {
			$method = [$method];
		}

		foreach ($method as $key => $value) {
			$method[$key] = mb_convert_case($value, MB_CASE_UPPER, "UTF-8");
		}

		$this->method = $method;

		return $this;
	}

	/**
	 * Set the callback function to execute if the route is a match
	 *
	 * @param callable $callback
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @throws \Exception
	 *
	 * @since 2.0.0
	 */
	public function callback($callback)
	{
		if (is_callable($callback)) {
			$this->callback = $callback;
		} else {
			throw new \Exception("The callback needs to be callable.", 500);
		}

		return $this;
	}

	/**
	 * Add a pattern a parametre should match
	 *
	 * @param string $parametre parametre name
	 * @param string $pattern   regular pattern to match the parametre (without delimiters)
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @since 2.0.0
	 */
	public function assert($parametre, $pattern)
	{
		$this->assert[$parametre] = $pattern;

		return $this;
	}

	/**
	 * Set a rule for converting a param into another data type
	 *
	 * @param string   $parametre
	 * @param callable $converter
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @since 2.0.0
	 */
	public function convert($parametre, $converter)
	{
		$this->convert[$parametre] = $converter;

		return $this;
	}

	/**
	 * Provide a default value for an optional parametre
	 *
	 * @param string $parametre Parametre name
	 * @param mixed  $value     Default value
	 *
	 * @return Route Provides a fluent interface
	 *
	 * @since 2.0.0
	 */
	public function value($parametre, $value)
	{
		$this->value[$parametre] = $value;

		return $this;
	}

	/**
	 * Run the route check
	 *
	 * @param string $uri      Request URI
	 * @param string $method   Request method
	 * @param string $protocol Either "HTTP", "HTTPS" or an empty string. Defaults to an empty string (match all).
	 *
	 * @return boolean True if the route is a match and a callback was executed, false otherwise
	 *
	 * @since 2.0.0
	 */
	public function run($uri = null, $method = null, $protocol = "")
	{
		// take care of null default values
		if ($method === null) {
			$method = isset($_SERVER["REQUEST_METHOD"]) ? $_SERVER["REQUEST_METHOD"] : "GET";
		}
		if ($uri === null) {
			if (isset($_SERVER["REQUEST_URI"])) {
				list($uri) = explode("?", $_SERVER["REQUEST_URI"]);
			} else {
				$uri = "/";
			}
		}

		// check request method
		$method = mb_convert_case($method, MB_CASE_UPPER, "UTF-8");
		if (!empty($this->method)) {
			if (!in_array($method, $this->method)) {
				return false;
			}
		}

		// check protocol
		if (mb_convert_case($protocol, MB_CASE_UPPER, "UTF-8") != mb_convert_case($this->protocol, MB_CASE_UPPER, "UTF-8")
			&& $protocol != ""
			&& $this->protocol != ""
		) {
			return false;
		}

		// compile a regex to match the URI and run the matching
		$compiled = $this->compilePattern();
		$isMatch = preg_match($compiled["regex"], $uri, $matches);

		// run the callback
		if ($isMatch) {
			// prepare the params
			$params = $this->value;;
			foreach ($compiled["variables"] as $variable) {
				if (array_key_exists($variable, $matches)) {
					$params[$variable] = $matches[$variable];
				}

				if ($this->hasConverter($variable)) {
					$params[$variable] = $this->runConverter($variable, $params[$variable]);
				}
			}

			if (is_callable($this->callback)) {
				return call_user_func($this->callback, $params);
			} else {
				return $params;
			}
		}

		return false;
	}

	/**
	 * Compile a regex that will match the URI pattern
	 *
	 * @return array
	 *
	 * @throws \Exception
	 *
	 * @since 2.0.0
	 */
	private function compilePattern()
	{
		if (empty($this->compiledPattern)) {
			$tokens = array();
			$variables = array();
			$matches = array();
			$pos = 0;
			$defaultSeparator = '/';

			// Match all variables enclosed in "{}" and iterate over them. But we only want to match the innermost variable
			// in case of nested "{}", e.g. {foo{bar}}. This in ensured because \w does not match "{" or "}" itself.
			preg_match_all('#\{\w+\}#', $this->uriPattern, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
			foreach ($matches as $match) {
				$varName = substr($match[0][0], 1, -1);
				// get all static text preceding the current variable
				$precedingText = substr($this->uriPattern, $pos, $match[0][1] - $pos);
				$pos = $match[0][1] + strlen($match[0][0]);
				$precedingChar = strlen($precedingText) > 0 ? substr($precedingText, -1) : '';
				$isSeparator = '' !== $precedingChar && false !== strpos(static::SEPARATORS, $precedingChar);

				if (is_numeric($varName)) {
					throw new \Exception(sprintf('Variable name "%s" cannot be numeric in route pattern "%s". Please use a different name.', $varName, $this->uriPattern));
				}
				if (in_array($varName, $variables)) {
					throw new \Exception(sprintf('Route pattern "%s" cannot reference variable name "%s" more than once.', $this->uriPattern, $varName));
				}

				if ($isSeparator && strlen($precedingText) > 1) {
					$tokens[] = array('text', substr($precedingText, 0, -1));
				} elseif (!$isSeparator && strlen($precedingText) > 0) {
					$tokens[] = array('text', $precedingText);
				}

				$regexp = $this->getAssert($varName);
				if (null === $regexp) {
					$followingPattern = (string) substr($this->uriPattern, $pos);
					// Find the next static character after the variable that functions as a separator. By default, this separator and '/'
					// are disallowed for the variable. This default requirement makes sure that optional variables can be matched at all
					// and that the generating-matching-combination of URLs unambiguous, i.e. the params used for generating the URL are
					// the same that will be matched. Example: new Route('/{page}.{_format}', array('_format' => 'html'))
					// If {page} would also match the separating dot, {_format} would never match as {page} will eagerly consume everything.
					// Also even if {_format} was not optional the requirement prevents that {page} matches something that was originally
					// part of {_format} when generating the URL, e.g. _format = 'mobile.html'.
					$nextSeparator = $this->findNextSeparator($followingPattern);
					$regexp = sprintf(
						'[^%s%s]+',
						preg_quote($defaultSeparator, self::REGEX_DELIMITER),
						$defaultSeparator !== $nextSeparator && '' !== $nextSeparator ? preg_quote($nextSeparator, self::REGEX_DELIMITER) : ''
					);
					if (('' !== $nextSeparator && !preg_match('#^\{\w+\}#', $followingPattern)) || '' === $followingPattern) {
						// When we have a separator, which is disallowed for the variable, we can optimize the regex with a possessive
						// quantifier. This prevents useless backtracking of PCRE and improves performance by 20% for matching those patterns.
						// Given the above example, there is no point in backtracking into {page} (that forbids the dot) when a dot must follow
						// after it. This optimization cannot be applied when the next char is no real separator or when the next variable is
						// directly adjacent, e.g. '/{x}{y}'.
						$regexp .= '+';
					}
				}

				$tokens[] = array('variable', $isSeparator ? $precedingChar : '', $regexp, $varName);
				$variables[] = $varName;
			}

			if ($pos < strlen($this->uriPattern)) {
				$tokens[] = array('text', substr($this->uriPattern, $pos));
			}

			// find the first optional token
			$firstOptional = PHP_INT_MAX;
			for ($i = count($tokens) - 1; $i >= 0; $i--) {
				$token = $tokens[$i];
				if ('variable' === $token[0] && $this->hasValue($token[3])) {
					$firstOptional = $i;
				} else {
					break;
				}
			}

			// compute the matching regexp
			$regexp = '';
			for ($i = 0, $nbToken = count($tokens); $i < $nbToken; $i++) {
				$regexp .= $this->computeRegexp($tokens, $i, $firstOptional);
			}

			$this->compiledPattern = [
				'regex' => self::REGEX_DELIMITER.'^'.$regexp.'$'.self::REGEX_DELIMITER.'s',
				'variables' => $variables,
				'tokens' => $tokens,
				'firstOptional' => $firstOptional
			];
		}

		return $this->compiledPattern;
	}

	/**
	 * Returns the next static character in the URI pattern that will serve as a separator.
	 *
	 * @param string $pattern The route pattern
	 *
	 * @return string The next static character that functions as separator (or empty string when none available)
	 *
	 * @since 2.0.0
	 */
	private function findNextSeparator($pattern)
	{
		if ('' == $pattern) {
			// return empty string if pattern is empty or false (false which can be returned by substr)
			return '';
		}
		// first remove all placeholders from the pattern so we can find the next real static character
		$pattern = preg_replace('#\{\w+\}#', '', $pattern);

		return isset($pattern[0]) && false !== strpos(static::SEPARATORS, $pattern[0]) ? $pattern[0] : '';
	}

	/**
	 * Computes the regexp used to match a specific token. It can be static text or a subpattern.
	 *
	 * @param array   $tokens        The route tokens
	 * @param integer $index         The index of the current token
	 * @param integer $firstOptional The index of the first optional token
	 *
	 * @return string The regexp pattern for a single token
	 *
	 * @since 2.0.0
	 */
	private function computeRegexp(array $tokens, $index, $firstOptional)
	{
		$token = $tokens[$index];
		if ('text' === $token[0]) {
			// Text tokens
			return preg_quote($token[1], self::REGEX_DELIMITER);
		} else {
			// Variable tokens
			if (0 === $index && 0 === $firstOptional) {
				// When the only token is an optional variable token, the separator is required
				return sprintf('%s(?P<%s>%s)?', preg_quote($token[1], self::REGEX_DELIMITER), $token[3], $token[2]);
			} else {
				$regexp = sprintf('%s(?P<%s>%s)', preg_quote($token[1], self::REGEX_DELIMITER), $token[3], $token[2]);
				if ($index >= $firstOptional) {
					// Enclose each optional token in a subpattern to make it optional.
					// "?:" means it is non-capturing, i.e. the portion of the subject string that
					// matched the optional subpattern is not passed back.
					$regexp = "(?:$regexp";
					$nbTokens = count($tokens);
					if ($nbTokens - 1 == $index) {
						// Close the optional subpatterns
						$regexp .= str_repeat(")?", $nbTokens - $firstOptional - (0 === $firstOptional ? 1 : 0));
					}
				}

				return $regexp;
			}
		}
	}

	/**
	 * Check whether a given parametre has a defined default value
	 *
	 * @param string $parametre
	 *
	 * @return boolean
	 *
	 * @since 2.0.0
	 */
	private function hasValue($parametre)
	{
		return array_key_exists($parametre, $this->value);
	}

	/**
	 * Get the assertion regex for a specific parametre
	 *
	 * @param string $parametre
	 *
	 * @return string|null
	 *
	 * @since 2.0.0
	 */
	private function getAssert($parametre)
	{
		return $this->hasAssert($parametre) ? $this->assert[$parametre] : null;
	}

	/**
	 * Check whether a given parametre has a defined assertion regex
	 *
	 * @param string $parametre
	 *
	 * @return boolean
	 *
	 * @since 2.0.0
	 */
	private function hasAssert($parametre)
	{
		return array_key_exists($parametre, $this->assert);
	}

	/**
	 * Check whether a given parametre has a converter
	 *
	 * @param string $parametre
	 *
	 * @return boolean
	 *
	 * @since 2.0.0
	 */
	private function hasConverter($parametre)
	{
		return array_key_exists($parametre, $this->convert);
	}

	/**
	 * Run a converter for a given parametre using a value
	 *
	 * @param string $parametre
	 * @param string $value
	 *
	 * @return mixed
	 *
	 * @since 2.0.0
	 */
	private function runConverter($parametre, $value)
	{
		return $this->hasConverter($parametre) ? $this->convert[$parametre]($value) : null;
	}

	/**
	 * Assemble a URL based on the defined match pattern and passed in pattern param values
	 *
	 * @param array $parametres Array of parametres to be rendered in the URL
	 *
	 * @throws RouteException
	 *
	 * @return string
	 *
	 * @since 2.0.0
	 */
	public function assemble(array $parametres = [])
	{
		// find parametres
		$compiled = $this->compilePattern();
		$tokens = array_reverse($compiled["tokens"], true);
		$firstOptional = $compiled["firstOptional"];

		// the output URL
		$url = "";

		$stopOmitting = false;
		foreach ($tokens as $index => $token) {
			// text tokens are inserted verbatim and no more tokens may be omitted
			if ($token[0] == "text") {
				$stopOmitting = true;
				$url = $token[1] . $url;
				continue;
			}

			// if we'moved beyond the first optional parametre, stop attempting to omit
			if ($firstOptional > $index) {
				$stopOmitting = true;
			}

			// OK, we're dealing with a variable, so let's check if we can omit it
			if (!$stopOmitting
				&& (!array_key_exists($token[3], $parametres)
					|| (array_key_exists($token[3], $this->value) && $parametres[$token[3]] == $this->value[$token[3]])
				)
			) {
				if (array_key_exists($token[3], $parametres)) {
					unset($parametres[$token[3]]);
				}
				continue;
			}

			// aha, can't omit...
			$stopOmitting = true;

			// we need to make sure the required value is available
			if (array_key_exists($token[3], $parametres)) {
				$value = $parametres[$token[3]];
				unset($parametres[$token[3]]);
			} else if (array_key_exists($token[3], $this->value)) {
				$value = $this->value[$token[3]];
			} else {
				throw new RouteException(RouteException::MISSING_ASSEMBLE_PARAMS);
			}

			// finally, we can prepend the parametre
			$url = $token[1] . $value . $url;
		}

		// the URL needs to at least be a slash
		if (!$url) {
			$url = "/";
		}

		// OK, if there are any parametres left, append them as a query string
		if (!empty($parametres)) {
			$url .= "?" . http_build_query($parametres);
		}

		return $url;
	}
}
